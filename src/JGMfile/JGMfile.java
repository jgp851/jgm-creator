package JGMfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import frame.MainFrame;
import jgp.std.JGPMath;

public class JGMfile {
	private String playlistTitle;

	ArrayList<JGMitem> itemList;

	public JGMfile() {
		itemList = new ArrayList<JGMitem>();
	}
	public JGMfile(String path) {
		itemList = new ArrayList<JGMitem>();
		readFile(path);
	}
	
	public JGMitem getItem(int id) {
		return itemList.get(id);
	}
	
	public String getPlaylistTitle() {
		return playlistTitle;
	}
	
	public boolean addItemFile(String path) {
		File file = new File(path);
		try {
				FileInputStream fs = new FileInputStream(file);
				DataInputStream ds = new DataInputStream(fs);
				int fileLength = ds.available();
				byte[] fileBuffer = new byte[fileLength];
				ds.read(fileBuffer);
				itemList.add(new JGMitem(fileBuffer, path));
			
				ds.close();
				fs.close();
				return true;
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}
	
	public boolean getHadItemBuffer(int id) {
		return itemList.get(id).getHadItemBuffer();
	}
	
	public int getItemCount() {
		return itemList.size();
	}
	
	public void addItem() {
		itemList.add(new JGMitem());
	}
	public void deleteItem(int id) {
		itemList.remove(id);
	}

	private void parsingData() {
		int currPos = 4 + JGPMath.stringByteLength(playlistTitle) + 4 * 4 * itemList.size(); // omini�cie warto�ci przed g��wnym nag�owkiem
		// obliczenie informacji o nag��wkach - headerAddress i headerLength
		for(int i = 0; i < itemList.size(); i++) {
			itemList.get(i).setHeaderAddress(currPos);
			int headerLength = JGPMath.stringByteLength(itemList.get(i).getTitle());
			headerLength += JGPMath.stringByteLength(itemList.get(i).getAlbum());
			headerLength += JGPMath.stringByteLength(itemList.get(i).getPerformer());
			headerLength += JGPMath.stringByteLength(itemList.get(i).getOrigin());
			headerLength += JGPMath.stringByteLength(itemList.get(i).getFileType());
			itemList.get(i).setHeaderLength(headerLength);
			currPos += headerLength;
		}
		// obliczenie informacji o nag��wkach - fileAddress i fileLength
		for(int i = 0; i < itemList.size(); i++) {
			// obliczenie informacji na podstawie uprzednio wczytanego item z pliku JGM
			itemList.get(i).setFileAddress(currPos);
			currPos += itemList.get(i).getFileLength();
		}
	}
	public void readFile(String path) {
		try {
			File file = new File(path);
			FileInputStream fs = new FileInputStream(file);
			DataInputStream ds = new DataInputStream(fs);
			int itemCount = JGPMath.little2big(ds.readInt());
			playlistTitle = ds.readUTF();
						
			// utworzenie obiekt�w JGMitem i zapisanie do listy itemList
			for(int i = 0; i < itemCount; i++)
				itemList.add(new JGMitem());
			// odczytywanie lokalizacje nag��wk�w items
			for(int i = 0; i < itemList.size(); i++)
				itemList.get(i).readHeaderLocation(ds);
			// odczytywanie nag��wk�w items
			for(int i = 0; i < itemList.size(); i++)
				itemList.get(i).readHeader(ds);
			// odczytywanie items
			for(int i = 0; i < itemList.size(); i++)
				itemList.get(i).readItem(ds);
			ds.close();
			fs.close();
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			if(ex.getMessage() != null)
				JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void setDataFromInterface(DefaultTableModel tableModel, String playlistTitle) {
		this.playlistTitle = playlistTitle;
		for(int i = 0; i < itemList.size(); i++) {
			itemList.get(i).setTitle((String) tableModel.getValueAt(i, MainFrame.TITLE_COLUMN));
			itemList.get(i).setAlbum((String) tableModel.getValueAt(i, MainFrame.ALBUM_COLUMN));
			itemList.get(i).setPerformer((String) tableModel.getValueAt(i, MainFrame.PERFORMER_COLUMN));
			itemList.get(i).setOrigin((String) tableModel.getValueAt(i, MainFrame.ORIGIN_COLUMN));
		}
	}
	public void writeFile(String path) {
		try {
			parsingData();

			File file = new File(path);
			FileOutputStream fs = new FileOutputStream(file);
			DataOutputStream ds = new DataOutputStream(fs);

			ds.writeInt(JGPMath.little2big(itemList.size()));
			ds.writeUTF(playlistTitle);
			// zapis lokalizacji nag��wk�w items
			for(int i = 0; i < itemList.size(); i++)
				itemList.get(i).writeHeaderLocation(ds);
			// zapis nag��wk�w items
			for(int i = 0; i < itemList.size(); i++)
				itemList.get(i).writeHeader(ds);
			// zapis items
			for(int i = 0; i < itemList.size(); i++)
				itemList.get(i).writeItem(ds);
			ds.close();
			fs.close();
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, "B�ad w zapisie pliku: " + path + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
