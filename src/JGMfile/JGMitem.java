package JGMfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import jgp.std.JGPMath;

public class JGMitem {
	private int headerAddress;
	private int headerLength;
	private int fileAddress;
	private int fileLength;
	
	private boolean hadItemBuffer; // flaga przechowuj�ca informacj� o fakcie przechowywania strumienia pliku d�wi�kowego w []itemBuffer

	private String title;
	private String album;
	private String performer;
	private String origin;
	private String fileType;
	
	private byte[] itemBuffer;
	
	public JGMitem() {}
	public JGMitem(byte[] itemBuffer, String filePath) {
		this.itemBuffer = itemBuffer;
		fileLength = itemBuffer.length;
		hadItemBuffer = true;
		setFileType(filePath);
	}
	

	public boolean getHadItemBuffer() {
		return hadItemBuffer;
	}
	public int getFileLength() {
		return fileLength;
	}
	
	public String getTitle() {
		return title;
	}
	public String getAlbum() {
		return album;
	}
	public String getPerformer() {
		return performer;
	}
	public String getOrigin() {
		return origin;
	}
	public String getFileType() {
		return fileType;
	}

	public void setAlbum(String album) {
		this.album = album;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public void setPerformer(String performer) {
		this.performer = performer;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setFileAddress(int fileAddress) {
		this.fileAddress = fileAddress;
	}
	public void setFileLength(int fileLength) {
		this.fileLength = fileLength;
	}
	public void setHeaderAddress(int headerAddress) {
		this.headerAddress = headerAddress;
	}
	public void setHeaderLength(int headerLength) {
		this.headerLength = headerLength;
	}
	
	public void readHeader(DataInputStream ds) throws IOException {
		title = ds.readUTF();
		album = ds.readUTF();
		performer = ds.readUTF();
		origin = ds.readUTF();
		fileType = ds.readUTF();
	}
	public void readHeaderLocation(DataInputStream ds) throws IOException {
		headerAddress = ds.readInt();
		headerLength = ds.readInt();
		fileAddress = ds.readInt();
		fileLength = ds.readInt();
		
		headerAddress = JGPMath.little2big(headerAddress);
		headerLength = JGPMath.little2big(headerLength);
		fileAddress = JGPMath.little2big(fileAddress);
		fileLength = JGPMath.little2big(fileLength);
	}
	public void readItem(DataInputStream ds) throws IOException {
		itemBuffer = new byte[fileLength];
		ds.read(itemBuffer);
		hadItemBuffer = true;
	}
	
	private void setFileType(String filePath) {
		fileType = filePath.substring(filePath.lastIndexOf("."));
	}

	public void writeHeader(DataOutputStream ds) throws IOException {
		ds.writeUTF(title);
		ds.writeUTF(album);
		ds.writeUTF(performer);
		ds.writeUTF(origin);
		ds.writeUTF(fileType);
	}
	public void writeHeaderLocation(DataOutputStream ds) throws IOException {
		ds.writeInt(JGPMath.little2big(headerAddress));
		ds.writeInt(JGPMath.little2big(headerLength));
		ds.writeInt(JGPMath.little2big(fileAddress));
		ds.writeInt(JGPMath.little2big(fileLength));
	}
	public void writeItem(DataOutputStream ds) throws IOException {
		ds.write(itemBuffer);
	}
}
