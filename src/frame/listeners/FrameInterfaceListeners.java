package frame.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.MainFrame;

public class FrameInterfaceListeners {
	private MainFrame frame;
	
	public FrameInterfaceListeners(MainFrame frame) {
		this.frame = frame;
	}

	public AddActionListener getAddActionListener() {
		return new AddActionListener();
	}
	public DeleteActionListener getDeleteActionListener() {
		return new DeleteActionListener();
	}
	
	class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.addItem();
		}
	}
	class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.deleteItem();
		}
	}
}
