package frame.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.MainFrame;

public class FrameMenuBarListeners {
	private MainFrame frame;
	
	public FrameMenuBarListeners(MainFrame frame) {
		this.frame = frame;
	}

	public ExitItemListener getExitItemListener() {
		return new ExitItemListener();
	}
	public NewItemListener getNewItemListener() {
		return new NewItemListener();
	}
	public OpenItemListener getOpenItemListener() {
		return new OpenItemListener();
	}
	public SaveItemListener getSaveItemListener() {
		return new SaveItemListener();
	}
	public SaveAsItemListener getSaveAsItemListener() {
		return new SaveAsItemListener();
	}
	
	class ExitItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.dispose();
		}
	}
	class NewItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.createInterface();
		}
	}
	class OpenItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.createExistingFileFrame();
		}
	}
	class SaveItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.saveFile();
		}
	}
	class SaveAsItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			frame.saveFileAs();
		}
	}
}
