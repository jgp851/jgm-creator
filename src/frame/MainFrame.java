package frame;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import JGMfile.JGMfile;
import frame.listeners.FrameInterfaceListeners;
import frame.listeners.FrameMenuBarListeners;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 2630322136338626255L;

	private FrameInterfaceListeners frameInterfaceListeners;
	private FrameMenuBarListeners frameMenuBarListeners;
	
	private JPanel panel;
	private JTable table;
	private JScrollPane tableScroll;
	private JMenuItem optionNew;
	private JMenuItem optionOpen;
	private JMenuItem optionSave;
	private JMenuItem optionSaveAs;
	private JMenuItem optionExit;
	private JTextField playlistTitle;
	private DefaultTableModel tableModel;
	private JGMfile jgmFile;
	private String currentFilePath;
	private String defaultFrameTitle;

	public static final int ID_COLUMN = 0;
	public static final int PATH_COLUMN = 1;
	public static final int TITLE_COLUMN = 2;
	public static final int ALBUM_COLUMN = 3;
	public static final int PERFORMER_COLUMN = 4;
	public static final int ORIGIN_COLUMN = 5;
	
	public MainFrame(String title) {
		super(title);
		defaultFrameTitle = title;
		
		frameInterfaceListeners = new FrameInterfaceListeners(this);
		frameMenuBarListeners = new FrameMenuBarListeners(this);

		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("Plik");
		optionNew = new JMenuItem("Nowy");
		optionOpen = new JMenuItem("Otw�rz");
		optionSave = new JMenuItem("Zapisz");
		optionSaveAs = new JMenuItem("Zapisz jako...");
		optionExit = new JMenuItem("Zako�cz");
		
		optionNew.addActionListener(frameMenuBarListeners.getNewItemListener());
		optionOpen.addActionListener(frameMenuBarListeners.getOpenItemListener());
		optionSave.addActionListener(frameMenuBarListeners.getSaveItemListener());
		optionSaveAs.addActionListener(frameMenuBarListeners.getSaveAsItemListener());
		optionExit.addActionListener(frameMenuBarListeners.getExitItemListener());
		fileMenu.add(optionNew);
		fileMenu.add(optionOpen);
		fileMenu.add(optionSave);
		fileMenu.add(optionSaveAs);
		fileMenu.add(optionExit);
		menuBar.add(fileMenu);
		this.setJMenuBar(menuBar);
		optionSave.setEnabled(false);
		optionSaveAs.setEnabled(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(480, 530);
		this.setVisible(true);
	}

	private DefaultTableModel getDefaultTableModel() {
		DefaultTableModel model = new DefaultTableModel() {
			private static final long serialVersionUID = 8019946646782922381L;
			public Class<?> getColumnClass(int column) {
				switch(column)
				{
				case ID_COLUMN: return Integer.class;
				case PATH_COLUMN: case TITLE_COLUMN: case ALBUM_COLUMN: case PERFORMER_COLUMN: case ORIGIN_COLUMN: return String.class;
				default: return Object.class;
				}
			}
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return columnIndex == PATH_COLUMN || columnIndex == TITLE_COLUMN || columnIndex == ALBUM_COLUMN || columnIndex == PERFORMER_COLUMN || columnIndex == ORIGIN_COLUMN;
			}
		};
		model.addColumn("ID");
		model.addColumn("�cie�ka");
		model.addColumn("Tytu�");
		model.addColumn("Album");
		model.addColumn("Wykonawca");
		model.addColumn("Pochodzenie");
		return model;
	}
	private String checkEmptyRows() {
		boolean noString = true;
		String msg = "";
		for(int i = 0; i < tableModel.getRowCount(); i++)
			if(tableModel.getValueAt(i, TITLE_COLUMN).toString().trim().isEmpty() ||
					tableModel.getValueAt(i, ALBUM_COLUMN).toString().trim().isEmpty() ||
					tableModel.getValueAt(i, PERFORMER_COLUMN).toString().trim().isEmpty() ||
					tableModel.getValueAt(i, ORIGIN_COLUMN).toString().trim().isEmpty()) {
				if(noString) {
					msg = "String: ";
					noString = false;
					msg += (i + 1);
				}
				else
					msg += ", " + (i + 1);
			}
		return msg;
	}
	private boolean saveIfEmptyRow() {
		int option = JOptionPane.YES_OPTION;
		String msg = checkEmptyRows();
		if(!msg.isEmpty())
			option = JOptionPane.showConfirmDialog(null, "Tabela zawiera puste wpisy. Zapisa� plik mimo to?\n" + msg, "Question", JOptionPane.YES_NO_OPTION);
		switch(option) {
		case JOptionPane.YES_OPTION: return true;
		case JOptionPane.NO_OPTION: return false;
		default: return false;
		}
	}
	public void addItem() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(new FileNameExtensionFilter("Audio files (*.wav, *.ogg, *.flac)", "wav", "ogg", "flac"));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	File file = chooser.getSelectedFile();
			optionSaveAs.setEnabled(true);
			if(jgmFile.addItemFile(file.getPath()))
				tableModel.addRow(new Object[]{null, new String(file.getPath()), null, null, null, null});
		}
	}
	public void createExistingFileFrame() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(new FileNameExtensionFilter("JGM file (*.jgm)", "jgm"));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	File file = chooser.getSelectedFile();
 			createInterface();
 			createJGMfile(file.getPath());
 			initTable();
        }
	}
	public void createInterface() {
		if(table == null)
			createNewInterface();
		else
			resetInterface();
	}
	private void createJGMfile(String path) {
		if(path != null) {
			setFrameTitle(path);
			setCurrentFilePath(path);
			jgmFile = new JGMfile(path);
			optionSave.setEnabled(true);
			optionSaveAs.setEnabled(true);
		}
		else {
			setFrameTitle(null);
			setCurrentFilePath(null);
			jgmFile = new JGMfile();
			optionSave.setEnabled(false);
			optionSaveAs.setEnabled(false);
		}
	}
	private void createNewInterface() {	
		if(jgmFile == null)
			createJGMfile(null);
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JPanel inputPanel = new JPanel();
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		
		JButton addButton = new JButton("Dodaj");
		addButton.addActionListener(frameInterfaceListeners.getAddActionListener());
		JButton deleteButton = new JButton("Usu�");
		deleteButton.addActionListener(frameInterfaceListeners.getDeleteActionListener());
		playlistTitle = new JTextField();
		playlistTitle.setColumns(15);
		btnPanel.add(addButton);
		btnPanel.add(deleteButton);
		btnPanel.add(playlistTitle);
		
		inputPanel.add(btnPanel);
		panel.add(inputPanel);
		createTable();
		this.getContentPane().add(BorderLayout.CENTER, panel);
		this.setVisible(true);
	}
	private void createTable() {
		tableModel = getDefaultTableModel();
		table = new JTable();
		prepareTable(tableModel);
		tableScroll = new JScrollPane(table);
		JPanel tablePanel = new JPanel();
		tablePanel.add(tableScroll);
		panel.add(tablePanel);
	}
	public void deleteItem() {
		try {
			int row = table.getSelectedRow();
			if(row != -1) {
				tableModel.removeRow(row);
				jgmFile.deleteItem(row);
				// zaznaczenie s�siedniego wpisu
				if(tableModel.getRowCount() > row)
					table.setRowSelectionInterval(row, row);
				else if(row > 0)
					table.setRowSelectionInterval(row - 1, row - 1);
			}
			else {
				if(table.getRowCount() == 0)
					throw new Exception("Tabela jest pusta!");
				else
					throw new Exception("Nie zaznaczono �adnego wpisu!");
			}
			if(table.getRowCount() == 0) {
				optionSave.setEnabled(false);
				optionSaveAs.setEnabled(false);
			}
		} catch(Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	private void initTable() {
		playlistTitle.setText(jgmFile.getPlaylistTitle());
		for(int i = 0; i < jgmFile.getItemCount(); i++)
			tableModel.addRow(new Object[]{null, null, jgmFile.getItem(i).getTitle(), jgmFile.getItem(i).getAlbum(), jgmFile.getItem(i).getPerformer(), jgmFile.getItem(i).getOrigin()});
	}
	private void prepareTable(DefaultTableModel model) {
		table.setModel(model);
		table.setRowHeight(25);
		table.getColumnModel().getColumn(ID_COLUMN).setCellRenderer(new TableIDColRenderer());
		table.addComponentListener(new TableListener());
	}
	private void resetInterface() {
		currentFilePath = null;
		tableModel = getDefaultTableModel();
		prepareTable(tableModel);
		resetJGMfile();
	}
	private void resetJGMfile() {
		createJGMfile(null);
	}
	public void saveFile() {
		if(jgmFile != null && currentFilePath != null) {
			// wyj�cie z trybu edycji
			if(table.isEditing())
				table.getCellEditor().stopCellEditing();
			if(saveIfEmptyRow()) {
				jgmFile.setDataFromInterface(tableModel, playlistTitle.getText());
				jgmFile.writeFile(currentFilePath);
			}
		}
		else
			JOptionPane.showMessageDialog(null, "B��d w zapisie pliku!", "Error", JOptionPane.ERROR_MESSAGE);
	}
	public void saveFileAs() {
		if(jgmFile != null) {
			// wyj�cie z trybu edycji
			if(table.isEditing())
				table.getCellEditor().stopCellEditing();
			if(saveIfEmptyRow()) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("JGM file (*.jgm)", "jgm"));
				int returnVal = chooser.showSaveDialog(this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					setCurrentFilePath(file.getPath());
					jgmFile.setDataFromInterface(tableModel, playlistTitle.getText());
					jgmFile.writeFile(file.getPath() + ".jgm");
					setFrameTitle(file.getName() + ".jgm");
				}
			}
		}
		else
			JOptionPane.showMessageDialog(null, "Nie utworzono pliku!", "Error", JOptionPane.ERROR_MESSAGE);
	}
	private void setCurrentFilePath(String path) {
		currentFilePath = path;
		// zapezpieczenie przed ustawieniem �cie�ki do pliku bez jego rozszerzenia
		if(currentFilePath != null && currentFilePath.lastIndexOf(".jgm") != currentFilePath.length() - 4)
			currentFilePath += ".jgm";
		if(path == null)
			optionSave.setEnabled(false);
		else
			optionSave.setEnabled(true);
	}
	private void setFrameTitle(String fileName) {
		if(fileName != null) {
			if(fileName.lastIndexOf('\\') != -1)
				fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
			this.setTitle(defaultFrameTitle + " - " + fileName);
		}
		else
			this.setTitle(defaultFrameTitle);
	}
	
	private class TableListener implements ComponentListener {
		private int lastRowCount = 0;

		@Override
		public void componentResized(ComponentEvent arg0) {
			// sprawdzenie czy dodano nowy wpis - je�li poprzednia warto�� rowCount jest mniejsza od bie��cej to znaczy, �e tak.
			if(lastRowCount < table.getRowCount()) {
		        table.clearSelection(); // odznaczanie ewentualnie zaznaczonych cells
				// po dodaniu stringu, automatyczne ustawienie si� na jego edycj�
				table.editCellAt(tableModel.getRowCount() - 1, PATH_COLUMN);
				((JTextField) table.getEditorComponent()).grabFocus();
				// przej�cie do ostatnio dodanego rekordu
		        JScrollBar vertical = tableScroll.getVerticalScrollBar();
		        vertical.setValue(vertical.getMaximum());
			}
			lastRowCount = table.getRowCount();
		}

		@Override
		public void componentHidden(ComponentEvent arg0) {}
		@Override
		public void componentMoved(ComponentEvent arg0) {}
		@Override
		public void componentShown(ComponentEvent arg0) {}
	}
	private class TableIDColRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = -8922616013829570659L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
			JLabel id = new JLabel(String.valueOf(row + 1));
			return id;
		}
	}
}
