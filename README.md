**JGM specification of file**

94 "JGM" 1A 0A 15 0D 0A

0  | 94 - liczba kontrolna moich plików

1  | "JGM" - identyfikator ASCII formatu pliku

4  | 1A - znak końca pliku. Zakończenie przypadkowego wypisywania pliku przez narzędzia przeznaczone do plików tekstowych.

5  | 0A - bajt LF końca linii typu Unix (nawiązanie do pliku PNG)

6  | 0D 0A - bajty CR LF końca linii typu Windows (nawiązanie jw.)

8  | 15 - bajt NEL (next line). Znak końca linii w systemach opartych na EBCDIC (nawiązanie jw.)

9  | 4 bajty - wersja pliku, liczba całkowita

13 | 3 bajty - wolne bajty - domyślnie "jgp"

-----

// struktura pliku

- int - generowany losowo identyfikator pliku playlisty

- int - liczba utworów

- string UTF-8 - nazwa playlisty

- nagłówek drugorzędny przechowujący dla każdego utworu adres głównego nagłówka, jego długość oraz podstawowe informacje o pliku audio - jego adres w pliku i długość:

 a) headerAddress (int)

 b) headerLength (int)

 c) fileAddress (int)

 d) fileLength (int)

- główny nagłowek przechowujący dla każdego utworu informacje o pliku audio:

 a) title (string),

 b) album (string),

 c) performer (string),

 d) origin - informacja o pochodzeniu utworu (np. soundtrack itp.) (string),

 e) fileType (string),

- kolejno wszystkie pliki audio (byte[])

// pozostałe informacje

- obsługiwane formaty: WAV, OGG, FLAC,

- Pamiętaj że każda zmienna łańcuchowa (string) poprzedzona jest dwu-bajtową liczbą short zawierającą informację o długości stringu. Długości te są zapisane w konwencji big-endian, więc konieczna jest ich konwersja do little-endian.

- Wszystkie wartości liczbowe prócz długości stringów są skonwertowane do little-endian.